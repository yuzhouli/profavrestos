package com.bignerdranch.android.criminalintent;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.android.criminalintent.yelpapi.APISearch;

public class CrimeFragment extends Fragment {
    public static final String EXTRA_CRIME_ID = "criminalintent.CRIME_ID";
    private static final int REQUEST_PHOTO = 1;

    Crime mCrime;
    EditText mTitleField;
    EditText mTitleField2;

    View v;

    Button mExtractButton, mCancelButton, mSaveButton;

    TextView mRestaurantNameTextView, mRestaurantAddressTextView, mRestaurantPhoneTextView;



    Callbacks mCallbacks;

    public interface Callbacks {
        void onCrimeUpdated(Crime crime);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    public static CrimeFragment newInstance(UUID crimeId) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CRIME_ID, crimeId);

        CrimeFragment fragment = new CrimeFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        UUID crimeId = (UUID)getArguments().getSerializable(EXTRA_CRIME_ID);
        mCrime = CrimeLab.get(getActivity()).getCrime(crimeId);

        setHasOptionsMenu(true);
    }


    @Override
    @TargetApi(11)
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_crime, parent, false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        
        mTitleField = (EditText)v.findViewById(R.id.crime_title);
        mTitleField.setText(mCrime.getTitle());
        mTitleField.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence c, int start, int before, int count) {
                mCrime.setTitle(c.toString());
                mCallbacks.onCrimeUpdated(mCrime);
            }

            public void beforeTextChanged(CharSequence c, int start, int count, int after) {
                // this space intentionally left blank
            }

            public void afterTextChanged(Editable c) {
                // this one too
            }
        });

        mTitleField2 = (EditText)v.findViewById(R.id.crime_city);
        mTitleField2.setText(mCrime.getTitle2());
        mTitleField2.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence c, int start, int before, int count) {
                mCrime.setTitle2(c.toString());
                mCallbacks.onCrimeUpdated(mCrime);
            }

            public void beforeTextChanged(CharSequence c, int start, int count, int after) {
                // this space intentionally left blank
            }

            public void afterTextChanged(Editable c) {
                // this one too
            }
        });

        mExtractButton = (Button)v.findViewById(R.id.extract_detail);
        mExtractButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mRestaurantNameTextView = (TextView) v.findViewById(R.id.yelp_list_item_restaurantNameTextView);
                mRestaurantAddressTextView = (TextView) v.findViewById(R.id.yelp_list_item_restaurantAddressTextView);
                mRestaurantPhoneTextView = (TextView) v.findViewById(R.id.yelp_list_item_restaurantPhoneTextView);



                String name = mCrime.getTitle();
                String location = mCrime.getTitle2();

                new APISearchTask().execute(name, location);
            }
        });

        mCancelButton = (Button)v.findViewById(R.id.cancel);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        mSaveButton = (Button)v.findViewById(R.id.save);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getActivity().finish();
            }
        });



        
        return v; 
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        if (requestCode == REQUEST_PHOTO) {
            // create a new Photo object and attach it to the crime
            String filename = data
                .getStringExtra(CrimeCameraFragment.EXTRA_PHOTO_FILENAME);
            if (filename != null) {
                Photo p = new Photo(filename);
                mCrime.setPhoto(p);
                mCallbacks.onCrimeUpdated(mCrime);
            }
        }
    }
    


    @Override
    public void onPause() {
        super.onPause();
        CrimeLab.get(getActivity()).saveCrimes();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(getActivity());
                return true;
            case R.id.menu_item_photo:
                // launch the camera activity
                Intent i = new Intent(getActivity(), CrimeCameraActivity.class);
                startActivityForResult(i, REQUEST_PHOTO);
                return true;
            case R.id.menu_item_extract:
                mRestaurantNameTextView = (TextView) v.findViewById(R.id.yelp_list_item_restaurantNameTextView);
                mRestaurantAddressTextView = (TextView) v.findViewById(R.id.yelp_list_item_restaurantAddressTextView);
                mRestaurantPhoneTextView = (TextView) v.findViewById(R.id.yelp_list_item_restaurantPhoneTextView);



                String name = mCrime.getTitle();
                String location = mCrime.getTitle2();

                new APISearchTask().execute(name, location);
                return true;
            case R.id.menu_item_exit:
                getActivity().finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        } 
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_resto_detail, menu);
    }

    private class APISearchTask extends AsyncTask<String, Void, ArrayList<String>> {

        @Override
        protected ArrayList<String> doInBackground(String... strings) {

            String name = strings[0];
            String location = strings[1];
            ArrayList<String> result = new ArrayList<String>();

            APISearch yelpApi = new APISearch();

            try {
                yelpApi.search(name, location);
            } catch (Exception e) {
                e.printStackTrace();
            }

            result.add(yelpApi.getResName());
            result.add(yelpApi.getResAddress());
            result.add(yelpApi.getResPhone());

            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<String> strings) {
            mRestaurantNameTextView.setText(strings.get(0));
            mRestaurantAddressTextView.setText(strings.get(1));
            mRestaurantPhoneTextView.setText(strings.get(2));
        }
    }
}
